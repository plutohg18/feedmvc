//
//  mockWebClient.swift
//  activityFeedTests
//
//  Created by Ge Huang on 27/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import Foundation
@testable import activityFeed

class MockWebClient : WebClient {

	override func baseRequest(completion: @escaping baseRequestCompletionBlock, resultCompletion: @escaping ItemsResultCompletionBlock) {
		let testBundle = Bundle(for: type(of: self))
		let path = testBundle.path(forResource: "sample", ofType: "json")
		if let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped) {
			_ = try? JSONSerialization.jsonObject(with: data, options: [])
		completion(data, nil , resultCompletion)
		}
	}

	override func urlRequest(_ url: URL, completion: @escaping urlRequestCompletionBlock, resultCompletion: @escaping ImageCompletionBlock) {
		let testBundle = Bundle(for: type(of: self))
		let path = testBundle.path(forResource: "sample", ofType: "jpg")
		if let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped) {
			_ = try? JSONSerialization.jsonObject(with: data, options: [])
			completion(data, nil , resultCompletion)
		}
	}
}
