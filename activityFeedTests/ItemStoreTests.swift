//
//  activityFeedTests.swift
//  activityFeedTests
//
//  Created by Ge Huang on 22/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import XCTest
@testable import activityFeed

class ItemStoreTests: XCTestCase {

	func testFetchingAllItems() {

		let expect = expectation(description: "Expect all items to be fetched")

		let itemStore = ItemStore(webClient: MockWebClient())

		itemStore.fetchItems { itemsResult in
			switch itemsResult {
			case let .success(items):
				XCTAssertEqual(items.count, 10)
				expect.fulfill()
			case let .failure(error):
				XCTAssertTrue(false, "\(error)")
			}
		}

		waitForExpectations(timeout: 5)
	}

	func testFetchingImage() {

		let expect = expectation(description: "Expect a image to be fetched")

		let itemStore = ItemStore(webClient: MockWebClient())

		itemStore.fetchImage(URL(string: "http://www.abc.net.au/news/image/10166710-4x3-140x105.jpg")!) { imageResult in
			switch imageResult {
			case let .success(image):
				XCTAssertNotNil(image)
				expect.fulfill()
			case let .failure(error):
				XCTAssertTrue(false, "\(error)")
			}
		}

		waitForExpectations(timeout: 5)
	}
}
