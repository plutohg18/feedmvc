//
//  ActivityViewController.swift
//  activityFeed
//
//  Created by Ge Huang on 2/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit

class ActivityViewController: UITableViewController {

	static let kTopCellIdentifier = "topCell"
	static let kRegularCellIdentifier = "regularCell"

	var itemStore: ItemStore!

    override func viewDidLoad() {
        super.viewDidLoad()

		// add logo to navigation bar
		let logo = UIImage(named: "logo.png")
		let imageView = UIImageView(image:logo)
		self.navigationItem.titleView = imageView

		// allowing auto-sizing table view
		tableView.rowHeight = UITableView.automaticDimension
		tableView.estimatedRowHeight = 265

		refresh()
    }


    // MARK: - Table view data source

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemStore.itemsCount()
    }

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		// return top cell for fisrt row and regular cell for the rest of table
		let identifier = indexPath.row == 0 ? ActivityViewController.kTopCellIdentifier : ActivityViewController.kRegularCellIdentifier

		let item = itemStore.itemForRow(indexPath.row)
		let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ActivityCell
			cell.titleLabel.text = item.title
			cell.dateLabel.text = item.formattedDateString
			loadCellImage(item, cell: cell)
		return cell
	}

	func loadCellImage(_ item: Item, cell : ActivityCell) {
		// some item may not have a image url
		guard let imageURL = item.imageURL else
		{
			return
		}

		guard let image = item.image else {
			itemStore.fetchImage(imageURL) { result in
				switch result {
				case let .success(image):
					cell.updateImage(image)
				case let .failure(error):
					print("Error fetching image: \(error)")
				}
			}
			return
		}

		cell.updateImage(image)
	}

	// MARK: - Push to refresh
	@IBAction func handleRefresh(_ sender: UIRefreshControl){
		refresh()
	}

	func refresh() {
		// refresh itemStore and table view
		itemStore.fetchItems {
			[unowned self] itemsResult in
			switch itemsResult {
			case let .success(items):
				self.itemStore.resetItems(items)
				self.tableView.reloadData()
				// dismis refresh control if needed
				self.dismissRefreshControl()
			case let .failure(error):
				print("Error fetching items: \(error)")
			}
		}
	}

	private func dismissRefreshControl() {
		// dismis refresh control if needed
		if let refreshControl = self.refreshControl, refreshControl.isRefreshing {
			refreshControl.endRefreshing()
		}
	}

	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segue.identifier {
		case "showWebView":
			if let selectedIndexPath =
				tableView.indexPathsForSelectedRows?.first {
				let item = itemStore.itemForRow(selectedIndexPath.row)
				let destination = segue.destination as! WebViewController
				destination.linkURL = item.linkURL
			}
		default:
			preconditionFailure("Unexpected segue identifier.")
		}
	}
}
