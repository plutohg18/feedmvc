//
//  Item.swift
//  activityFeed
//
//  Created by Ge Huang on 2/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit

class Item: NSObject {

	var title : String
	var imageURL: URL?
	var publishedDate: Date
	var linkURL: URL
	var image: UIImage?

	// string representation of publish date in expected output format
	var formattedDateString : String {
		get {
			return Item.dateFormatter.string(from: publishedDate)
		}
	}

	private static let dateFormatter: DateFormatter = {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
		return dateFormatter
	}()

	init(title: String, imageURL: URL?, publishedDate: Date, linkURL: URL) {
		self.title = title
		self.imageURL = imageURL
		self.publishedDate = publishedDate
		self.linkURL = linkURL
	}
}

