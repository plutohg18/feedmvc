//
//  JSONParser.swift
//  activityFeed
//
//  Created by Ge Huang on 2/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import Foundation

enum ParseError: Error {
	case invalidJSONData
}

struct JSONParser {
	
	// date formatter to interpret input data
	static let dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
		return formatter
	}()

	// parse JSON to array of items
	static func items(fromJSON data: Data) -> ItemsResult {
		do {
			let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])

			guard let jsonDictionary = jsonObject as? [AnyHashable:Any],
				let itemJSONs = jsonDictionary["items"] as? [[String : Any]] else {
				// The JSON structure doesn't match our expectations
				return .failure(ParseError.invalidJSONData)
			}

			var items = [Item]()
			var isFirst = true
			for itemJSON in itemJSONs {
				if let item = item(itemJSON, isFirst: isFirst) {
					items.append(item)
				}
				isFirst = false
			}

			if items.isEmpty {
				// We weren't able to parse any of the items
				return .failure(ParseError.invalidJSONData)
			}
			return .success(items)
		} catch let error {
			return .failure(error)
		}
	}

	// converted each JSON to a item
	private static func item(_ json: [String : Any], isFirst : Bool) -> Item? {
		guard
			let title = json["title"] as? String,
			let dateString = json["pubDate"] as? String,
			let linkURLString = json["link"] as? String,
			let linkURL = URL(string: linkURLString),
			let dateTaken = dateFormatter.date(from: dateString) else {
				// Don't have enough information to construct a Item
				return nil
		}

		// get enclosure link url for the first item, and thumbnail url for the rest
		var url : URL?
		if isFirst {
			if let enclosure = json["enclosure"] as? [String : Any], let linkString = enclosure["link"] as? String {
				url = URL(string: linkString)
			}
		} else {
			if let thumbnailURLString = json["thumbnail"] as? String {
				url = URL(string: thumbnailURLString)
			}
		}

		return Item(title: title, imageURL: url, publishedDate: dateTaken, linkURL: linkURL)
	}
}
