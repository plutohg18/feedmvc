//
//  ActivityCell.swift
//  activityFeed
//
//  Created by Ge Huang on 2/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit

class ActivityCell: UITableViewCell {
	@IBOutlet var customImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!

	func updateImage(_ image: UIImage?) {
		if image != nil {
			customImageView.image = image
		} else {
			customImageView.image = nil
		}
	}

	override func prepareForReuse() {
		super.prepareForReuse()

		updateImage(nil)
		titleLabel.text = nil
		dateLabel.text = nil
	}	 
}
